# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Online host for the BECI estimation of aboveground biomass
 
### How to use script ###
Save script to parent folder of where .LAS files are located.
Change the 'lasfiles' variable in BECI_EST_AGB.py to whatever the name of the folder holding your .LAS files.
Optional: 
To use with plot data, first uncomment all code that is not a "comment". Update plot data file path to the 
full path name.

### Who do I talk to? ###

John Dilger
johnjdilger@gmail.com